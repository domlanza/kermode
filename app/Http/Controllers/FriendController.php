<?php

namespace Kermode\Http\Controllers;

use Auth;
use Kermode\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
  public function getIndex()
  {
    $friends = Auth::user()->friends();
    $requests = Auth::user()->friendRequests();

    return view('friends.index')
      ->with('friends', $friends)
      ->with('requests', $requests);
  }

  public function getAdd($first_name)
  {
    $user = User::where('first_name', $first_name)->first();

    if (!$user) {
      return redirect()
        ->route('home')
        ->with('info', 'That user could not be found');
    }

    if (Auth::user()->id == $user->id){
      return redirect()->route('home');
    }

    if (Auth::user()->hasFriendRequestPending($user) || $user->
        hasFriendRequestPending(Auth::user())) {
          return redirect()
            ->route('profile.index', ['first_name' => $user->first_name])
            ->with('info', 'Friend request already pending.');

    }

    if (Auth::user()->isFriendsWith($user)) {
        return redirect()
        ->route('profile.index', ['first_name' => $user->first_name])
        ->with('info', 'You are already friends');

    }

    Auth::user()->addFriend($user);

    return redirect()
      ->route('profile.index', ['first_name' => $first_name])
      ->with('info', 'Friend request sent.');

  }
  public function getAccept($first_name)
  {
    $user = User::where('first_name', $first_name)->first();

    if (!$user) {
      return redirect()
        ->route('home')
        ->with('info', 'That user could not be found');
    }

    if(!Auth::user()->hasFriendRequestRecieved($user)){
      return redirect()->route('home');
    }
    Auth::user()->acceptFriendRequest($user);

    return redirect()
    ->route('profile.index', ['first_name' => $first_name])
    ->with('info', 'Friend request accepted.');

  }
}
