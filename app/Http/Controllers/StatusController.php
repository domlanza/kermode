<?php

namespace Kermode\Http\Controllers;

use Auth;
use Kermode\User;
use Illuminate\Http\Request;

class StatusController extends Controller
{
  public function postStatus(Request $request)
  {
     $this->validate($request, ['status' => 'required|trader_maxindex:1000',
     ]);
     Auth::user()->statuses()->create(['body' => $request->input('status'),
     ]);
     return redirect()
     ->route('home')
     ->with('info', 'Status poseted.');

  }
}
