<?php

namespace Kermode\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use Kermode\User;

class ProfileController extends Controller
{
    public function index() {

      return view ('profile.index' , array('user' => Auth::user()) );

    }

    public function update_avatar(Request $request){

      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time () . "," . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300, 300)->save( storage_path('/uploads/avatars/' . $filename ) );

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
    return view ('profile.index' , array('user' => Auth::user()) );
    }
    public function findFriends(){
        $uid = Auth::user()->id;
        $allUsers = DB::table('users')->where('id', '!=', $uid)->get();

        return view('profile.findFriends', compact($allUsers));
      }
    public function getProfile($first_name){
       $user = User::where('first_name', $first_name)->first();
       if(!$user){
         abort(404);
       }
       return view('profile.index') -> with('user', $user);

    }
    public function getEdit()
    {
        return view('profile.edit');
    }
    public function postEdit(Request $request)
    {
      $this->validate($request, [
        'first_name' => 'alpha|max:50',
        'last_name' => 'alpha|max:50',
      ]);
      Auth::user()->update([
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
      ]);
      return redirect()->route('profile.edit')->with('info', 'Your profile has been updated.');
    }

}
