<?php

namespace Kermode;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Status extends Authenticatable
{
    protected $table = 'statuses';
    protected $filltable = ['body'];
    public function user()
    {
      return $this->belongsTo('Kermode', 'user_id');
    }
}
