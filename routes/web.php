<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/findFriends', 'HomeController@findFriends');

Auth::routes();
  Route::group(['middleware'=> 'auth'], function () {

      Route::get('/home', 'HomeController@index')->name('home');
      Route::get('/alert', function () {
        return redirect()->route('home')->with('info', 'You have signed up!');
      });
      Route::get('/profile.index', 'ProfileController@index');
      Route::post('/profile.index', 'ProfileController@update_avatar');
      Route::get('/search', [
        'uses' => '\Kermode\Http\Controllers\SearchController@getResults',
        'as' => 'search.results',
      ]);
      Route::get('/profile/{first_name}', [
        'uses' => 'ProfileController@getProfile',
        'as' => 'profile.index',
      ]);
      Route::get('/profile.edit', [
        'uses' => 'ProfileController@getEdit',
        'as' => 'profile.edit',
      ]);
      Route::post('/profile.edit', [
        'uses' => 'ProfileController@postEdit',
      ]);
      Route::get('/friends', [
        'uses' => '\Kermode\Http\Controllers\FriendController@getIndex',
        'as' => 'friend.index',
      ]);
      Route::get('/friends/add/{first_name}', [
        'uses' => '\Kermode\Http\Controllers\FriendController@getAdd',
        'as' => 'friend.add',
      ]);
      Route::get('/friends/accept/{first_name}', [
        'uses' => '\Kermode\Http\Controllers\FriendController@getAccept',
        'as' => 'friend.accept',
      ]);
      Route::post('/status', [
        'uses' => '\Kermode\Http\Controllers\StatusController@postStatus',
        'as' => 'status.post',
      ]);

  });
