@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-5">
          @include('partials.userblock')
          <hr>
    </div>
    <div class="col-lg-4 col-lg-offset-3">
      @if (Auth::user()->hasFriendRequestPending($user))
        <p>Waitng for {{ $user->getNameOrUsername() }} to accept your
        request </p>
      @elseif (Auth::user()->hasFriendRequestRecieved($user))
        <a href="{{ route('friend.accept', ['first_name' => $user->first_name]) }}" class="btn btn-primary">Accept Friend Request</a>
      @elseif (Auth::user()->isFriendsWith($user))
        <p>You and {{ $user->getNameOrUsername() }} are friends. </p>
      @else
        <a href="{{ route('friend.add', ['first_name' => $user->first_name])
        }}" class="btn btn-primary">Add as friend</a>
      @endif
        <h4>{{ $user->getFirstNameOrUsername() }}'s friends.</h4>

        @if (!$user->friends()->count())
          <p>{{ $user->getFirstNameOrUsername() }} has no friends</p>
          @else
              @foreach ($user->friends() as $user)

                @include('partials.userblock')

              @endforeach
          @endif

    </div>
</div>
@endsection
