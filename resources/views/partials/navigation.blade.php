<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="{{ url('/') }}">Kermode</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    @auth
      <ul class="nav navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{route('home')}}">Timeline</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('friend.index')}}">Friends</a>
        </li>
      </ul>
      <form class="form-inline" role="search" action="{{ route('search.results') }}">
          <input type="text" name="query" class="form-control mr-2" placeholder="Find people">
          <button type="submit" class="btn btn-primary">Search</button>
      </form>
      <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
          @guest
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @else
              @if (Auth::check())
                  <li class="nav-item"><a class="nav-link" href="{{ route('profile.index', ['first_name' => Auth::user()->first_name]) }}">{{ Auth::user()->getNameOrUsername() }}</a></li>
              @endif
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="position:relative; padding-left:50px;">
                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:10px; border-radius:50%;">
                      {{ Auth::user()->first_name }} <span class="caret"></span>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item" href="/home">

                    {{ __('Home') }} </a>


                    <a class="dropdown-item" href="{{ route( 'profile.edit' ) }}">

                    {{ __('Profile') }} </a>


                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}

                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>



                  </div>
              </li>


          @endguest
      </ul>
    @endauth
          </div>
      </div>
  </nav>
